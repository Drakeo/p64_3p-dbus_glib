#!/bin/bash

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e
# complain about unset env variables
set -u

PROJECT="dbus-glib"
SOURCE_DIR="$PROJECT"
# e.g. PACKAGE_VERSION='0.76'
VERSION="$(sed -n -E "s/^ *PACKAGE_VERSION *= *'([0-9.]+)' *$/\1/p" \
               "$SOURCE_DIR/configure")"

DBUS_SOURCE_DIR="dbus"
# e.g. PACKAGE_VERSION='1.2.1'
DBUS_VERSION="$(sed -n -E "s/^ *PACKAGE_VERSION *= *'([0-9.]+)' *$/\1/p" \
                    "$DBUS_SOURCE_DIR/configure")"

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

# set LL_BUILD and friends... but this script doesn't build anything
#set_build_variables convenience Release

stage="$(pwd)/stage"
case "$AUTOBUILD_PLATFORM" in
    linux*)
        pushd "$SOURCE_DIR"
            # copy just the headers to the right place
            mkdir -p "$stage/include/dbus"
            cp -dp dbus/*.h "$stage/include/dbus"
        popd
        pushd "$DBUS_SOURCE_DIR"
            cp -dp dbus/*.h "$stage/include/dbus"
        popd
    ;;
    *)
        fail "dbus-glib headers only for linux"
    ;;
esac
mkdir -p "$stage/LICENSES"
tail -n 31 "$SOURCE_DIR/COPYING" > "$stage/LICENSES/$PROJECT.txt"
cp -v "${SOURCE_DIR}/doc/reference/version.xml" "${stage}/VERSION.txt"



